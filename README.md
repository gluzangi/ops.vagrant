# Vagrant Using Virtualbox and Ansible

The Vagrant Ansible provisioner allows you to provision the guest using Ansible playbooks by executing ansible-playbook from the Vagrant host.

## Setup Requirement
If installing Ansible directly on the Vagrant host is not an option in your development environment, you might be looking for the [Ansible Local provisioner](https://www.vagrantup.com/docs/provisioning/ansible_local.html) alternative

### Prerequisites

Use Ansible to install:

- MySQL
- Wordpress

### Additional notes:

- Setup the Linux box so that you can SSH into it using your SSH key
- Document your approach, your design and what would you have done differently if you had more time
- The code has to be deployable onto a Ubuntu 14.04 machine on the cloud. We use Azure. Assume that the server is already provided for you.
- Code should be checked into a git repository. Please share your repository with us.


### Essential Plugin For Cloud Deployment Includes:

- Install vagrant-aws plugin 
```
vagrant plugin install vagrant-aws
```
- Install vagrant-azure plugin 
```
vagrant plugin install vagrant-azure
```
- Install vagrant-digitalocean plugin 
```
vagrant plugin install vagrant-digitalocean
```

### For Azure:

- Install the Azure CLI in a vagrant host as it will help to retrieve certain information that is needed to create our machine. If a recent version of Node.js is installed, just run:
```
npm install -g azure-cli
```

- Use *Vagrant-Azure* plugin that adds Microsoft Azure provider to Vagrant to control and provision machines in Microsoft Azure.

### Running The Solution

- Start a vagrantbox with
```
vagrant up
```

- To configure the vagrantbox modify the Ansible Playbook and apply changes with
```
vagrant provision
```

## Built With

* [Vagrant](https://www.vagrantup.com/) - The Virtual Machine Solution
* [Vagrant-Azure](https://github.com/Azure/vagrant-azure) - Azure Provisioning
* [Ansible](https://maven.apache.org/) - Environment Orchestration Tool
* [MySQL](https://rometools.github.io/rome/) - Backend DB
* [Wordpress](https://rometools.github.io/rome/) - A CMS Solution

## Authors

* **Gerald Luzangi** - [gluzangi](https://github.com/gluzangi)


## Acknowledgments

* Hat tip to WE.org for Inspiration
